// BASE SETUP
// =============================================================================

// call the packages we need
var http = require("http");
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var ip   = process.env.IP;        // set our port
var port = process.env.PORT;        // set our port

var mongoose = require('mongoose');
//mongoose.connect("mongodb://"+ip+":27017/test");

mongoose.connect('mongodb://bvanberl:zorc11@apollo.modulusmongo.net:27017/ga4pimuD')

var User = require('./app/models/user');
var Post = require('./app/models/post');

// parse application/vnd.api+json as json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); 

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public')); 



// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Hooray! Welcome to our API!' });   
});

// more routes for our API will happen here
// on routes that end in /bears
// ----------------------------------------------------

//-----------------------------------------GOOGLE USERS------------------------------------------------------------------

router.route('/users')

    // create a user (accessed at POST http://localhost:8080/api/users)
    .post(function(req, res) {
        
        var user = new User();      // create a new instance of the Usermodel
        user.email = req.body.email;  // set the user's email (comes from the request)
        user.name = req.body.name;
        user.token = req.body.token;
        console.log("Added user " + user.email);

        // save the Google user and check for errors
        user.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });
        
    })
    
// get all the users (accessed at GET http://localhost:8080/api/users)
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err)
                res.send(err);

            res.json(users);
        });
    });

router.route('/users/:token')

    // get the bear with that id (accessed at GET http://localhost:8080/api/users/:bear_id)
    .get(function(req, res) {
        User.findById(req.params.token, function(err, user) {
            if (err)
                res.send(err);
            res.json(user);
        });
    })

 // update the bear with this id (accessed at PUT http://localhost:8080/api/bears/:bear_id)
    .put(function(req, res) {

        // use our user model to find the bear we want
        User.findById(req.params.token, function(err, user) {

            if (err)
                res.send(err);

            user.name = req.body.name;  // update the bears info

            // save the user
            user.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });

        });
    });
    

//-----------------------------------------POSTS-------------------------------------------------------------------------

router.route('/posts')

    // create a post (accessed at POST http://localhost:8080/api/posts/)
    .post(function(req, res) {
        
        var post = new Post();      // create a new instance of the Post model
        post.email = req.body.email;  
        post.text= req.body.text;
        post.ID - req.body.ID;
        post.dateTime = req.body.dateTime;
        console.log("Added post: " + post.text);

        // save the Facebook and check for errors
        post.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Post created!' });
        });
        
    })
    
// get all the posts (accessed at GET http://localhost:8080/api/posts)
    .get(function(req, res) {
        Post.find(function(err, posts) {
            if (err)
                res.send(err);

            res.json(posts);
        });
    })
    
// delete all posts
    .delete(function(req, res) {
        Post.remove({
        }, function(err, post) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });

router.route('/posts/:post_id')

    // get the post with that id (accessed at GET http://localhost:8080/api/users/:post_id)
    .get(function(req, res) {
        Post.findById(req.params.token, function(err, post) {
            if (err)
                res.send(err);
            res.json(post);
        });
    })

 // update the post with this id (accessed at PUT http://localhost:8080/api/bears/:post_id)
    .put(function(req, res) {

        // use our user model to find the bear we want
        Post.findById(req.params.token, function(err, post) {

            if (err)
                res.send(err);

             post.email = req.body.email;  
             post.text= req.body.text;
             post.ID - req.body.ID;
             post.dateTime = req.body.dateTime;

            // save the user
            post.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Post updated!' });
            });

        });
    });


    
// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
var server = http.createServer(app);
server.listen(port);
console.log('Magic happens on port ' + port);