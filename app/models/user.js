// grab the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema   = new Schema({
    email: {type : String, default: 'No_email'},
    name: {type : String, default: 'No_name'},
    token: {type : String, default: 'No_token'}
});

// define our user model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('User', UserSchema);