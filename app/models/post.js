// grab the mongoose module
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema   = new Schema({
    email: {type : String, default: 'No_email'},
    text: {type : String, default: 'No_text'},
    ID: {type : String, default: 'No_ID'},
    dateTime: {type : String, default: 'No_time'}
});

// define our user model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Post', PostSchema);