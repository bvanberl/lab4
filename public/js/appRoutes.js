    /*global angular*/
    angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        })
        
        .when('/main', {
            templateUrl: 'views/main.html',
            controller: 'MainController'
        })
        .otherwise({
            redirectTo: '/main'
      });

    $locationProvider.html5Mode(true);

}]);