/*global angular*/
/*global FB*/
angular.module('MainCtrl', []).controller('MainController', ['$scope', '$http', '$rootScope', '$route', function($scope, $http, $rootScope, $route) {
    
    var welcomeMsg = "Welcome!";
    document.getElementById("welcomeUser").innerHTML = welcomeMsg;
    
    var email = document.getElementById("email").innerHTML;
    email = email.substring(15);
    console.log("THe email " + email);

    $http.get('/api/posts/').success(function(response)
    {
        console.log(response);
        var counter = 1;
        for(var i = 0; i < response.length; i++)
        {
            console.log(response[i].email + " " + response[i].dateTime + " " + email);
            //if(response[i].email == email)
            {
                document.getElementById('postTableBody').innerHTML = document.getElementById('postTableBody').innerHTML + "<tr><td>" + counter + "</td><td>" + response[i].text + "</td><td>" + response[i].email + "</td><td>" + response[i].dateTime + "</td></tr>";
                ++counter;
            }
        }
    });
    
    $scope.signInFB = function(){
    FB.login(function(response) {
        if (response.authResponse) {
           FB.api('/me', function(response) {
             console.log('Name: ' + response.name + '.');
             $rootScope.facebookToken = FB.getAuthResponse().accessToken;
             $rootScope.facebookid = FB.getAuthResponse().userID;
             console.log("Access token: " + $rootScope.facebookToken + " and user id: " + $rootScope.facebookid);
             //load FB feeds
             //getFBFeeds();
             document.getElementById("facebookSignInLink").innerHTML = "<img src='img/facebook_32.png'/> Signed in as " + response.name;
         });
       } else {
           console.log('Login did not work.');
       }
   }, {
     scope: 'public_profile, publish_actions, user_posts', 
     return_scopes: true
        });
        
    }
    
    $scope.makePost = function(){
        var status = document.getElementById("postInput").value;
        var data, email, id, dateTime;
        FB.api('/me/feed', 'post', { message: status }, function(response) {
            if (!response || response.error) {
                alert('Error occured');
                return;
            } else {
                alert('Post ID: ' + response.id);
            
            console.log("data" + data);
            }
            $scope.ID = response.id;
        });
        
        email = document.getElementById("email").innerHTML;
        email = email.substring(15);
        var now = new Date();
        id = $scope.ID;
        dateTime = [[now.getDate(), now.getMonth() + 1, now.getFullYear()].join("/"), [now.getHours(), now.getMinutes()].join(":"), now.getHours() >= 12 ? "PM" : "AM"].join(" ");
        
        console.log(email + " " + dateTime + " " + status);
        data = {email: email, text: status, ID: id, dateTime: dateTime};
        $http.post('/api/posts/', data); // Add post to database
        
        $route.reload();
        
    }
 

}]);