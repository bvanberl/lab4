/*global angular*/
angular.module('LoginCtrl', []).controller('LoginController', ['$scope', '$http', '$location', '$rootScope', function($scope, $http, $location, $rootScope) {

    $scope.signedIn = false;
    $scope.firstName = "";
    $scope.lastName = "";
    $scope.email = "";
    
    var url = document.location.href;
    if(url.indexOf("token") != -1)
    {
        $scope.signedIn = true;
        
        var token = window.location.href;
        console.log("1. " + token);
        token = token.substring(token.indexOf('access_token=') + 13, token.indexOf('&'));
        console.log(token);
        
        $http.get('https://www.googleapis.com/plus/v1/people/me?access_token=' + token).success(function(response){
            var firstName = response.name.givenName;
            var lastName = response.name.familyName;
            var email = response.emails[0].value;
            var fullName = firstName + " " + lastName;
            $scope.email = email;
            $rootScope.fullName = fullName;
            
            var data = {email: response.emails[0].value, name: fullName, token: token};
            console.log("data" + data);
            $http.post('/api/users/', data); // Add user to database
            
            document.getElementById("welcome").innerHTML = "Welcome, " + fullName + ".";
            document.getElementById("email").innerHTML = "Signed in with " + email;
            
        });
    }
    
    
    $scope.onSignIn = function()
    {
        var client_id ="511736525485-68m20br7o7klprbq8oecin6fodrljc92.apps.googleusercontent.com";
        var scope="email";
        var redirect_uri = "https://lab4-bvanberl.c9users.io/";
        var response_type = "token";
    	var url="https://accounts.google.com/o/oauth2/auth?scope="+scope+"&client_id="+client_id+"&redirect_uri="+redirect_uri+"&response_type="+response_type;
    	

        //console.log(gapi.auth.getToken());
        
        window.location.replace(url);
    
    };
    

}]);

//Google+ API Key: AIzaSyCo2C9KoqtidqJAjlk-IIRoSTjKznWRWP0
// Client ID: 511736525485-68m20br7o7klprbq8oecin6fodrljc92.apps.googleusercontent.com
// Client secret: efw65qBCgLxkh3FHNGwEUsBJ

// Create post request send to server
// Authenticate in the server to check if token is valid