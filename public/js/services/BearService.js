/*global angular*/
angular.module('BearService', []).factory('Bear', ['$http', function($http) {

    return {
        // call to get all nerds
        get : function() {
            return $http.get('/api/bears/');
        },


        // these will work when more API routes are defined on the Node side of things
        // call to POST and create a new nerd
        create : function(bearData) {
            return $http.post('/api/bears/', bearData);
        },

        // call to DELETE a nerd
        delete : function(id) {
            return $http.delete('/api/bears/' + id);
        }
    }       

}]);

